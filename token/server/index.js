const FLAG = process.env.FLAG || 'flag{json_13}';
const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3000;

const express = require('express');
const morgan = require('morgan');

let app = express();
app.use(morgan(':remote-addr :method :url')); // logging
app.use('/', express.static('./'));

class TokenError extends Error { }

app.get('/flag', (req, res) => {
  try {
    let token = req.query.token;
    if (!token) throw new TokenError('¿Y el token?, ¿Donde esta el token?, ¿Te lo comiste?.');
    if (token.length > 24) throw new TokenError(`Bro, ${token.length} es muy largo`);
    if (0 < token.length < 8) throw new TokenError('No, yo por eso me quejo y quejo.');

  } catch (e) {
    if (e instanceof TokenError) {
      res.send(e.message);
      return;
    }
  }

  res.send(FLAG);
});

app.listen(PORT, HOST);
// https://github.com/djosix/AIS3-2019-Pre-Exam